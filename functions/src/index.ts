import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

admin.initializeApp();

export const correctUserTaskInput = functions.firestore
    .document("tasks/{taskId}")
    .onCreate((snap, context) => {
      const newTaskId = context.params.taskId;
      const newTask = snap.data();

      let taskName = newTask["title"];
      const beforeTaskName = taskName;

      taskName = taskName.replace(/ +(?= )/g, "");
      taskName = taskName.trim();
      taskName = taskName.charAt(0).toUpperCase() + taskName.slice(1);

      admin.firestore().collection("tasks").doc(newTaskId).update({
        title: taskName,
      }).then(() => {
        console.log(`Complete update ${beforeTaskName} to ${taskName}`);
      }).catch((err) => {
        console.log(`Error: ${err}`);
      });
    });
